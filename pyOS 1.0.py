
speed = 0.1
command_in = 0
num_a = 0
num_b = 0
check_turned_on = 1
def add(num_a, num_b):
    return int(num_a)+int(num_b)

def shutdown():
    return "OFF"

def sub(num_a, num_b):
    return(int(num_a)-int(num_b))

def help():
    return("in.add = addition, in.sub = substraction, shutdown = turn off pyOS")

def commands(command_in):
    if command_in == "help":
        return help()
    else:
        if command_in == "in.add":
            num_a = input("num a: ")
            num_b = input("num b: ")
            return add(num_a, num_b)
        else:
            if command_in == "shutdown":
                return shutdown()
            else:
                if command_in == "test":
                    return"test"
                else:
                    if command_in == "in.sub":
                        num_a = input("num a: ")
                        num_b = input("num_b: ")
                        return sub(num_a, num_b)
                    else:
                        return("unkown command, tipe help to see all avaiable commands")

print("welcome to pyOS 1.0! type help to see all avaiable commands")
while check_turned_on == 1:
    command_in = input("- ")
    if command_in[:2] == "in":
        print(commands(command_in))
    else:
        if commands(command_in) == "OFF":
            check_turned_on = 0
        else:
            print(commands(command_in))